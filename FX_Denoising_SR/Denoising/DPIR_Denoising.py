import os
import torch
import utils
import Denoising.utils_DPIR
import numpy as np

class DPIR_Denoising:
    def __init__(self):
        self.noise_level_img = 5  # set AWGN noise level for noisy image
        self.noise_level_model = self.noise_level_img  # set noise level for model
        self.model_name = 'drunet_color'  # set denoiser model, 'drunet_gray' | 'drunet_color'
        self.x8 = False  # default: False, x8 to boost performance


        self.__set_model()

    def __set_model(self):
        print("__set_model")

        if 'color' in self.model_name:
            self.n_channels = 3  # 3 for color image
        else:
            self.n_channels = 1  # 1 for grayscale image

        self.model_pool = 'Denoising/trained_model'  # fixed


        self.model_path = os.path.join(self.model_pool, self.model_name + '.pth')
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        torch.cuda.empty_cache()

        from Denoising.models.network_unet import UNetRes as net
        self.model = net(in_nc=self.n_channels + 1, out_nc=self.n_channels, nc=[64, 128, 256, 512], nb=4, act_mode='R',
                    downsample_mode="strideconv", upsample_mode="convtranspose")
        self.model.load_state_dict(torch.load(self.model_path), strict=True)
        self.model.eval()
        for k, v in self.model.named_parameters():
            v.requires_grad = False
        self.model = self.model.to(self.device)

    def preprocessing(self, img):
        print("preprocessing")
        float_img = utils.uint2single(img)
        # Add noise without clipping
        #np.random.seed(seed=0)  # for reproducibility
        #float_img += np.random.normal(0, self.noise_level_img/255., float_img.shape)

        tensor_img = utils.single2tensor4(float_img)
        print(img.shape)
        tensor_img = torch.cat(
            (tensor_img, torch.FloatTensor([self.noise_level_model / 255.]).repeat(1, 1, tensor_img.shape[2], tensor_img.shape[3])), dim=1)

        tensor_img = tensor_img.to(self.device)

        return tensor_img


    def run(self, img):
        print("denoising")
        if not self.x8 and img.size(2) // 8 == 0 and img.size(3) // 8 == 0:
            img_E = self.model(img)
        elif not self.x8 and (img.size(2) // 8 != 0 or img.size(3) // 8 != 0):
            img_E = Denoising.utils_DPIR.test_mode(self.model, img, refield=64, mode=5)
        elif self.x8:
            img_E = Denoising.utils_DPIR.test_mode(self.model, img, mode=3)

        img_E = utils.tensor2uint(img_E)

        img_E = np.squeeze(img_E)
        if img_E.ndim == 3:
            img_E = img_E[:, :, [2, 1, 0]]


        return img_E

