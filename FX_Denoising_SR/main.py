
import Denoising.DPIR_Denoising as Denoising
import SR.DASR_SR as SR
import utils
import os
import imageio

import cv2

def main():
    print("main")
    denoising = Denoising.DPIR_Denoising()
    sr = SR.DASR_SR()

    L_path = 'L_images'
    L_paths = utils.get_image_paths(L_path)
    for idx, img_full_name in enumerate(L_paths):
        img_name, ext = os.path.splitext(os.path.basename(img_full_name))
        print(img_name)
        img = utils.imread_uint(img_full_name, n_channels=3)

        denoise_result_img = denoising.preprocessing(img)
        denoise_result_img = denoising.run(denoise_result_img)
        cv2.imshow("Test_denoise", denoise_result_img)
        cv2.imwrite(os.path.join("SR_images/Denoising", img_name + '.png'), denoise_result_img)

        sr_result_img = sr.preprocessing(denoise_result_img)
        sr_result_img = sr.run(sr_result_img)

        cv2.imshow("Test_SR", sr_result_img)
        cv2.imwrite(os.path.join("SR_images/SR", img_name + '.png'), sr_result_img)
        cv2.waitKey(0)

if __name__ == '__main__':
    main()
