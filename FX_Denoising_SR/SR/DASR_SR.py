import os
import torch
from SR.models.blindsr import BlindSR
import utils
import numpy as np

class DASR_SR:
    def __init__(self):
        print("__init__")
        self.blur_type = 'aniso_gaussian' # iso_gaussian | aniso_gaussian
        self.scale = '4'

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') # torch.device('cpu')  #
        #self.device = torch.device('cpu')
        torch.cuda.empty_cache()

        self.__set_model()

    def __set_model(self):
        print("__set_model")
        dir = ''
        if self.blur_type == 'iso_gaussian':
            dir = './SR/trained_model/blindsr_x' + str(int(self.scale)) + '_bicubic_iso'
        elif self.blur_type == 'aniso_gaussian':
            dir = './SR/trained_model/blindsr_x' + str(int(self.scale)) + '_bicubic_aniso'

        self.DASR = BlindSR(self.scale).cuda()
        self.DASR.load_state_dict(torch.load(dir + '/model/model_600.pt'), strict=False)
        self.DASR.eval()

    def preprocessing(self, img):
        print("preprocessing")
        torch.cuda.memory_stats(device=0)
        torch.cuda.empty_cache()
        img = np.ascontiguousarray(img.transpose((2, 0, 1)))
        img = torch.from_numpy(img).float().cuda().unsqueeze(0).unsqueeze(0)

        return img


    def run(self, lr_img):
        print("super resolution")
        # inference
        sr_img = self.DASR(lr_img[:, 0, ...])
        sr_img = utils.quantize(sr_img, 255.0)

        sr_img = np.array(sr_img.squeeze(0).permute(1, 2, 0).data.cpu())
        
        #sr_img = sr_img[:, :,]
        sr_img = np.uint8(sr_img)

        return sr_img
